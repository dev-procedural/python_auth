# services/scores/project/tests/utils.py


from project import db
from project.api.models import SERVICE_MODEL

# example add something to model


def add_table(table_id):
    table = SERVICE_MODEL(
        table_id=table_id,
    )
    db.session.add(table)
    db.session.commit()
    return table
