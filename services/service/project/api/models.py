# services/scores/project/api/models.py


from project import db


class SERVICE_MODEL(db.Model):

    __tablename__ = 'SERVICE_MODEL'

    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    table_id = db.Column(db.Integer, nullable=False)

    def __init__(self, table_id, correct=False):
        self.table_id = table_id

    def to_json(self):
        return {
            'id': self.id,
            'table_id': self.table_id,
        }
