# services/order/project/api/order.py


# from sqlalchemy import exc
from flask import Blueprint, request, jsonify
from flask_restful import Resource, Api

# from project import db
# from project.api.models import order
from project.api.utils import authenticate_restful, authenticate


SERVICE_blueprint = Blueprint('SERVICE', __name__)
api = Api(SERVICE_blueprint)


@SERVICE_blueprint.route('/SERVICE/ping', methods=['GET'])
@authenticate
def ping_pong(resp):
    return jsonify({
        'status': 'success',
        'message': 'pong!'
    })


class SERVICE_api(Resource):

    method_decorators = {
                'post': [authenticate_restful],
                'get':  [authenticate_restful]}

    def get(self, resp):
        """GET"""
        response_object = {
            'status': 'success',
            'data': {
                'get': [
                    "SERVICE get"
                ]
            }
        }

        return response_object, 200

    # protected call
    def post(self, resp):
        """POST"""
        response_object = {
            'status': 'fail',
            'message': 'Invalid payload.'
        }
        post_data = request.get_json(force=True)
        if not resp['data']['admin']:
            response_object = {
                'status': 'error',
                'message': 'You do not have permission to do that.'
            }
            return response_object, 401

        post_data = request.get_json()
        if not post_data:
            return response_object, 400

        response_object['status']  = "passed"
        response_object['message'] = "pass post"
        response_object['data']    = {"post": post_data}

        return response_object, 200


api.add_resource(SERVICE_api, '/SERVICE')
